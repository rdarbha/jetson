# Jetson

## Setting up VNC


https://forums.developer.nvidia.com/t/setting-up-vnc-server-solved/65428


Edit the file `~/.config/autostart/vino-server.desktop`

```
[Desktop Entry]
Name=Desktop Sharing
Comment=GNOME Desktop Sharing Server
Keywords=vnc;share;remote;
Exec=/usr/lib/vino/vino-server --sm-disable
Icon=preferences-desktop-remote-desktop
NoDisplay=true
Terminal=false
Type=Application
X-GNOME-Autostart-Phase=Applications
X-GNOME-AutoRestart=true
X-GNOME-UsesNotifications=true
X-Ubuntu-Gettext-Domain=vino
```

```
:~/.config/autostart$ ls -al
total 12
drwxr-xr-x  2 rama rama 4096 Oct 27 19:09 .
drwx------ 21 rama rama 4096 Oct 27 19:08 ..
-rw-r--r--  1 rama rama  354 Oct 27 19:09 vino-server.desktop
```


## Resolution of VNC session using vino

### Using xorg.conf

`vino-server` uses X-server for resolution. By default, vino-server will default to 680x480. To change this, you can edit the default X11 config:

```
$ cat /etc/X11/xorg.conf
# Copyright (c) 2011-2013 NVIDIA CORPORATION.  All Rights Reserved.

#
# This is the minimal configuration necessary to use the Tegra driver.
# Please refer to the xorg.conf man page for more configuration
# options provided by the X server, including display-related options
# provided by RandR 1.2 and higher.

# Disable extensions not useful on Tegra.
Section "Module"
    Disable     "dri"
    SubSection  "extmod"
        Option  "omit xfree86-dga"
    EndSubSection
EndSection

Section "Device"
    Identifier  "Tegra0"
    Driver      "nvidia"
# Allow X server to be started even if no display devices are connected.
    Option      "AllowEmptyInitialConfiguration" "true"
EndSection

Section "Screen"
   Identifier    "Default Screen"
   Monitor       "Configured Monitor"
   Device        "Default Device"
   SubSection "Display"
       Depth    24
       Virtual 1280 800
   EndSubSection
EndSection
```

Add the section at the bottom:

```
Section "Screen"
   Identifier    "Default Screen"
   Monitor       "Configured Monitor"
   Device        "Default Device"
   SubSection "Display"
       Depth    24
       Virtual 1280 800
   EndSubSection
EndSection
```

### using xrandr

xrandr will dynamically change the resolution but it won't make it persistent.

```$ xrandr --fb 1600x1200```